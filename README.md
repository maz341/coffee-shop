# Coffee Shop (React Native)

Coffee Shop is a mobile app made using react native that displays a list of coffee items fetched from an API. The user can view details of each coffee item, mark them as favorites, and order them. The app features a visually appealing design with images and animations, and provides a smooth user experience for exploring and ordering coffee items.


## 🛠 Skills

React Native, Typescript, Styled-components, Expo

## Demo

<div align='center'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/one.gif' width='200' alt='Demo GIF'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/two.gif' width='200' alt='Demo GIF'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/three.gif' width='200' alt='Demo GIF'>
</div>

## Screenshots


<div align='center'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/1.png' width='200' alt='Demo Screenshot'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/2.png' width='200' alt='Demo Screenshot'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/3.png' width='200' alt='Demo Screenshot'>
</div>

## Demo Video

<div align='center'>
  <img src='https://gitlab.com/maz341/coffee-shop/-/raw/main/assets/demo/video.mp4'  alt='Demo Video'>

</div>

## 🔗 Links

[![Website](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://maazkamal.com)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/mazkamal/)
[![Gitlab](https://img.shields.io/badge/gitlab-fc6d27?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/maz341)
