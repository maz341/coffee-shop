export const AppImage = {
  coffeeShopBg:
    'https://manual.co.id/wp-content/uploads/2017/09/Say-Something-Como-Cafe_ComoPark-8-980x719.jpg',
  coffeePNG:
    'https://static.vecteezy.com/system/resources/previews/029/283/233/original/coffee-coffee-coffee-clipart-transparent-background-ai-generative-free-png.png',
  coffeePNG2:
    'https://static.vecteezy.com/system/resources/thumbnails/026/791/497/small/watercolor-cup-of-coffee-illustration-ai-generative-png.png',
  coffeePNG3:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Coffee_cup_icon.svg/2137px-Coffee_cup_icon.png',
  userAvatar:
    'https://media.licdn.com/dms/image/D5603AQFQm-Vn0wY19Q/profile-displayphoto-shrink_200_200/0/1707871661983?e=2147483647&v=beta&t=Wh8Qlc_O2uLVeumkz23CL9Wca-5I3cmz_pAuvYF0idY',
};
