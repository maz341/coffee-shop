export const Colors = {
  lightBrownColor: '#c6915d',
  darkBrownColor: '#533a20',
  lightTextColor: '#c6915d',
  darkTextColor: '#533a20',
};
