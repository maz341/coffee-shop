export interface ErrorType {
  message: string;
  myStatusCode: string;
}
