export interface DataItem {
    id: number;
    name: string;
    // Other properties
}

// Define interfaces for other responses
